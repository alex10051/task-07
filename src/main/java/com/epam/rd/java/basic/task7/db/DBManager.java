package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static final String URL;
	private static final String GET_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login = ?";
	private static final String GET_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name = ?";
	private static final String GET_TEAMS_FOR_USER = "SELECT id, name FROM teams WHERE id IN (SELECT team_id FROM users_teams WHERE user_id = ?)";
	private static final String GET_ALL_USERS = "SELECT id, login FROM users";
	private static final String INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
	private static final String INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private static final String GET_ALL_TEAMS = "SELECT id, name FROM teams";
	private static final String ADD_USER_TEAM_CON = "INSERT INTO users_teams VALUES (?, ?)";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE name=?";
	private static final String DELETE_USER = "DELETE FROM users WHERE login=?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

	static {
		Properties properties = load();
		URL = properties.getProperty("connection.url");
	}

	public static synchronized DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(URL);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(GET_ALL_USERS)) {

			while (rs.next()) {
				User user = User.createUser(rs.getString(2));
				user.setId(rs.getInt(1));
				users.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Getting users error", e);
		}

		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);) {
			stmt.setString(1, user.getLogin());
			int count = stmt.executeUpdate();
			if(count > 0)
				try(ResultSet rs = stmt.getGeneratedKeys()) {
					if(rs.next())
						user.setId(rs.getInt(1));
				}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(DELETE_USER)) {
			for(User user : users) {
				stmt.setString(1, user.getLogin());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = User.createUser(login);
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(GET_USER_BY_LOGIN);) {
			stmt.setString(1, login);
			stmt.executeQuery();
			try(ResultSet rs = stmt.getResultSet()) {
				if(rs.next())
					user.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = Team.createTeam(name);
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(GET_TEAM_BY_NAME);) {
			stmt.setString(1, name);
			stmt.executeQuery();
			try(ResultSet rs = stmt.getResultSet()) {
				if(rs.next())
					team.setId(rs.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(URL);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(GET_ALL_TEAMS)) {

			while (rs.next()) {
				Team team = Team.createTeam(rs.getString(2));
				team.setId(rs.getInt(1));
				teams.add(team);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Getting teams error", e);
		}

		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);) {
			stmt.setString(1, team.getName());
			int count = stmt.executeUpdate();
			if(count > 0)
				try(ResultSet rs = stmt.getGeneratedKeys()) {
					if(rs.next())
						team.setId(rs.getInt(1));
				}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try{
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			stmt = con.prepareStatement(ADD_USER_TEAM_CON);
			stmt.setString(1, Integer.toString(user.getId()));
			con.setAutoCommit(false);
			for(Team team : teams) {
				stmt.setString(2, Integer.toString(team.getId()));
				stmt.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(con);
			throw new DBException("Transaction failed", e);
		} finally {
			close(stmt);
			close(con);
		}

		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try(Connection con = DriverManager.getConnection(URL);
			PreparedStatement stmt = con.prepareStatement(GET_TEAMS_FOR_USER);) {

			stmt.setString(1, Integer.toString(user.getId()));
			try (ResultSet rs = stmt.executeQuery()){
				while (rs.next()) {
					Team team = Team.createTeam(rs.getString(2));
					team.setId(rs.getInt(1));
					teams.add(team);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException("Getting teams for user " + user.getLogin() + " error", e);
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(DELETE_TEAM)) {
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection con = DriverManager.getConnection(URL);
			 PreparedStatement stmt = con.prepareStatement(UPDATE_TEAM)) {
			stmt.setString(1, team.getName());
			stmt.setString(2, Integer.toString(team.getId()));
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static Properties load() {
		Properties props = new Properties();
		try(InputStream is = new FileInputStream("app.properties")) {
			props.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return props;
	}

	private void rollback(Connection con) {
		try{
			con.rollback();
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void close(AutoCloseable ac) {
		try{
			ac.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
